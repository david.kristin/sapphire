<?php

use App\Models\Project;
use App\Models\Status;
use App\Models\Task;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    $term = $faker->optional()->dateTimeInInterval('now');

    return [
        'title'       => $faker->words(3, true),
        'description' => $faker->sentences(3, true),
        'term'        => is_null($term) ? null : $term->format('Y-m-d H:i:s'),
        'duration'    => $faker->optional()->time(),
        'assignee_id' => factory(User::class)->lazy(),
        'author_id'   => factory(User::class)->lazy(),
        'project_id'  => factory(Project::class)->lazy(),
        'status_id'   => factory(Status::class)->lazy(),
    ];
});
