<?php

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name'        => $faker->unique()->words(3, true),
        'description' => $faker->optional()->sentences(3, true),
        'user_id'     => factory(User::class)->lazy(),
    ];
});

$factory->afterCreatingState(Project::class, 'members', function (Project $project) {
    $members = factory(User::class, 3)
        ->create()
        ->pluck('id')
        ->toArray();

    $project
        ->members()
        ->attach($members);
});

$factory->afterCreatingState(Project::class, 'tasks', function (Project $project) {
    factory(Task::class, 3)->create([
        'project_id' => $project->id,
    ]);
});
