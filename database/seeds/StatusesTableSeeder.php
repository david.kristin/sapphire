<?php

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Status::count() === 0) {
            Status::create([
                'id'   => Status::BACKLOG,
                'name' => 'Backlog',
            ]);

            Status::create([
                'id'   => Status::TODO,
                'name' => 'To-Do',
            ]);

            Status::create([
                'id'   => Status::SPRINT,
                'name' => 'Sprint',
            ]);

            Status::create([
                'id'   => Status::PROGRESS,
                'name' => 'Progress',
            ]);

            Status::create([
                'id'   => Status::TESTING,
                'name' => 'Testing',
            ]);

            Status::create([
                'id'   => Status::DONE,
                'name' => 'Done',
            ]);
        }
    }
}
