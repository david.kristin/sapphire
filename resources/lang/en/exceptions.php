<?php

return [
    'invalid_credentials' => 'The email and password did not match our records.',
];
