<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')
    ->name('v1.')
    ->namespace('V1')
    ->group(function () {
        Route::group(['middleware' => 'auth:api'],  function () {
            Route::delete('tokens', 'TokenController@destroy')
                ->name('logout');

            Route::put('tokens', 'TokenController@update')
                ->name('refresh');

            Route::get('users/{user}', 'UserController@show')
                ->name('profile');

            Route::put('users/{user}', 'UserController@update')
                ->name('updateProfile');

            Route::put('users/{user}/passwords', 'PasswordController@update')
                ->name('changePassword');

            Route::get('projects', 'ProjectController@index')
                ->name('listProjects');

            Route::post('projects', 'ProjectController@store')
                ->name('createProject');
            
            Route::delete('projects/{project}', 'ProjectController@destroy')
                ->name('deleteProject');

            Route::put('projects/{project}/members', 'MemberController@update')
                ->name('updateMembers');

            Route::get('projects/{project}/tasks', 'TaskController@index')
                ->name('listTasks');

            Route::post('projects/{project}/tasks', 'TaskController@store')
                ->name('createTask');
        });

        Route::post('users', 'UserController@store')
            ->name('register');

        Route::post('tokens', 'TokenController@store')
            ->name('login');
    });
