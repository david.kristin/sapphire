<?php declare(strict_types = 1);

namespace App\ValueObjects;

use App\Contracts\ValueObject;
use App\Exceptions\NegativeIdException;
use App\Traits\HasSingleValue;

final class Id implements ValueObject
{
    use HasSingleValue;

    /** @var int */
    private $value;

    public function __construct(int $value)
    {
        throw_if($value < 0, NegativeIdException::class);

        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function toNative(): int
    {
        return $this->value;
    }
}
