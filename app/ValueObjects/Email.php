<?php declare(strict_types = 1);

namespace App\ValueObjects;

use App\Contracts\ValueObject;
use App\Exceptions\InvalidEmailException;
use App\Traits\HasSingleValue;

final class Email implements ValueObject
{
    use HasSingleValue;

    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        $email = filter_var($value, FILTER_VALIDATE_EMAIL);

        throw_if($email === false, InvalidEmailException::class);

        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function toNative(): string
    {
        return $this->value;
    }
}
