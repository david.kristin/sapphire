<?php declare(strict_types = 1);

namespace App\ValueObjects;

use App\Contracts\ValueObject;
use App\Exceptions\PasswordTooShortException;
use App\Traits\HasSingleValue;

final class Password implements ValueObject
{
    use HasSingleValue;

    /** @var string */
    private $value;

    public function __construct(string $value)
    {
        throw_if(strlen($value) < 8, PasswordTooShortException::class);

        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function toNative(): string
    {
        return $this->value;
    }
}
