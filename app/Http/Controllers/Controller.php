<?php declare(strict_types = 1);

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Execute the given interaction.
     *
     * @param string $interaction
     * @param array<int, mixed>  $parameters
     * @return mixed
     * @throws \Exception
     */
    protected function interact(string $interaction, array $parameters)
    {
        $callback = [app($interaction), 'handle'];

        if (! is_callable($callback)) {
            throw new \Exception();
        }

        return call_user_func_array($callback, $parameters);
    }
}
