<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\ProjectRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProjectMembers;
use App\Models\Project;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

final class MemberController extends Controller
{
    /** @var \App\Contracts\Repositories\ProjectRepository */
    private $project;

    /** @var \Illuminate\Contracts\Routing\ResponseFactory */
    private $response;

    public function __construct(ProjectRepository $project, ResponseFactory $response)
    {
        $this->project = $project;
        $this->response = $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateProjectMembers $request
     * @param \App\Models\Project                     $project
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectMembers $request, Project $project): Response
    {
        $this->project->sync(
            $project->id,
            'members',
            $request->input('members')
        );

        return $this->response->noContent();
    }
}
