<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\ProjectRepository;
use App\Criteria\ProjectsByUserCriteria;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProject;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use App\ValueObjects\Id;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

final class ProjectController extends Controller
{
    /** @var \App\Contracts\Repositories\ProjectRepository */
    private $project;

    /** @var \Illuminate\Contracts\Auth\Guard */
    private $auth;

    /** @var \Illuminate\Contracts\Routing\ResponseFactory */
    private $response;

    public function __construct(ProjectRepository $project, Guard $auth, ResponseFactory $response)
    {
        $this->project = $project;
        $this->auth = $auth;
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $this->project->pushCriteria(
            new ProjectsByUserCriteria(
                new Id((int) $this->auth->id())
            )
        );

        $projects = $this->project->all();

        return ProjectResource::collection($projects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CreateProject $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProject $request): Response
    {
        $this->project->create($request->toArray());

        return $this->response->noContent(SymfonyResponse::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project): Response
    {
        $this->authorize('delete', $project);

        $this->project->delete($project->id);

        return $this->response->noContent();
    }
}
