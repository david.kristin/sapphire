<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\InvalidCredentialsException;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignInUser;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

final class TokenController extends Controller
{
    /** @var \Illuminate\Contracts\Auth\Guard|\Tymon\JWTAuth\JWTGuard */
    private $auth;

    /** @var \Illuminate\Contracts\Config\Repository */
    private $config;

    /** @var \Illuminate\Contracts\Routing\ResponseFactory */
    private $response;

    public function __construct(Guard $auth, Repository $config, ResponseFactory $response)
    {
        $this->auth = $auth;
        $this->config = $config;
        $this->response = $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\SignInUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SignInUser $request): JsonResponse
    {
        $token = $this->auth->attempt($request->validated());

        throw_if(! $token, InvalidCredentialsException::class);

        return $this->response->json(
            $this->tokenPayload((string) $token),
            SymfonyResponse::HTTP_CREATED
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(): JsonResponse
    {
        $token = $this->auth->refresh();

        return $this->response->json(
            $this->tokenPayload($token)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(): Response
    {
        $this->auth->logout(true);

        return $this->response->noContent();
    }

    /**
     * @param string $token
     * @return array<string, array<string, string|int|float>>
     */
    private function tokenPayload(string $token): array
    {
        return [
            'data' => [
                'access_token' => $token,
                'token_type'   => 'bearer',
                'expires_in'   => $this->config->get('jwt.ttl') * 60,
            ],
        ];
    }
}
