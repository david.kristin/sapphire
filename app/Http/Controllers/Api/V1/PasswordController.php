<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePassword;
use App\Models\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

final class PasswordController extends Controller
{
    /** @var \App\Contracts\Repositories\UserRepository */
    private $user;

    /** @var \Illuminate\Contracts\Routing\ResponseFactory */
    private $response;

    public function __construct(UserRepository $user, ResponseFactory $response)
    {
        $this->user = $user;
        $this->response = $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\ChangePassword $request
     * @param \App\Models\User                  $user
     * @return \Illuminate\Http\Response
     */
    public function update(ChangePassword $request, User $user): Response
    {
        $this->user->update(
            ['password' => bcrypt($request->input('password'))],
            $user->id
        );

        return $this->response->noContent();
    }
}
