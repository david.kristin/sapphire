<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Interactions\StoreTask;
use App\Contracts\Repositories\TaskRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTask as CreateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

final class TaskController extends Controller
{
    /** @var \App\Contracts\Repositories\TaskRepository */
    private $task;

    /** @var \Illuminate\Contracts\Routing\ResponseFactory */
    private $response;

    public function __construct(TaskRepository $task, ResponseFactory $response)
    {
        $this->task = $task;
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Project $project): AnonymousResourceCollection
    {
        $this->authorize('view', [Task::class, $project]);

        $tasks = $this->task->findByField('project_id', $project->id);

        return TaskResource::collection($tasks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     * @param \App\Http\Requests\CreateTask $request
     * @param \App\Models\Project           $project
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request, Project $project): Response
    {
        $this->interact(StoreTask::class, [$request->toDto()]);

        return $this->response->noContent(SymfonyResponse::HTTP_CREATED);
    }
}
