<?php declare(strict_types = 1);

namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignUpUser;
use App\Http\Requests\UpdateProfile;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

final class UserController extends Controller
{
    /** @var \App\Contracts\Repositories\UserRepository */
    private $user;

    /** @var \Illuminate\Contracts\Routing\ResponseFactory */
    private $response;

    public function __construct(UserRepository $user, ResponseFactory $response)
    {
        $this->user = $user;
        $this->response = $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\SignUpUser $request
     * @return \Illuminate\Http\Response
     */
    public function store(SignUpUser $request): Response
    {
        $this->user->create($request->toArray());

        return $this->response->noContent(SymfonyResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\User $user
     * @return \App\Http\Resources\UserResource
     */
    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateProfile $request
     * @param \App\Models\User                 $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfile $request, User $user): Response
    {
        $this->user->update(
            $request->toArray(),
            $user->id
        );

        return $this->response->noContent();
    }
}
