<?php declare(strict_types = 1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array<string, string|int|\Illuminate\Http\Resources\Json\JsonResource>
     */
    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'title'    => $this->title,
            'assignee' => new UserResource($this->assignee),
            'status'   => new StatusResource($this->status),
        ];
    }
}
