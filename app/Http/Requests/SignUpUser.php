<?php declare(strict_types = 1);

namespace App\Http\Requests;

use App\DataTransferObjects\User;
use Illuminate\Foundation\Http\FormRequest;

class SignUpUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'name'     => 'required|alpha_dash|unique:users',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return $this
            ->toDto()
            ->toArray();
    }

    /**
     * Get the DTO representation of the request.
     *
     * @return \App\DataTransferObjects\User
     */
    public function toDto(): User
    {
        return User::make(
            $this->name,
            $this->email,
            $this->password
        );
    }
}
