<?php declare(strict_types = 1);

namespace App\Http\Requests;

use App\DataTransferObjects\Profile;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('update', $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<int, string|object>>
     */
    public function rules(): array
    {
        $user = $this->user();

        return [
            'name'  => [
                'required',
                Rule::unique('users')->ignore($user),
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return $this
            ->toDto()
            ->toArray();
    }

    /**
     * Get the DTO representation of the request.
     *
     * @return \App\DataTransferObjects\Profile
     */
    public function toDto(): Profile
    {
        return Profile::make(
            $this->name,
            $this->email
        );
    }
}
