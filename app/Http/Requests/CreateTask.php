<?php declare(strict_types = 1);

namespace App\Http\Requests;

use App\DataTransferObjects\Task;
use App\Models\Task as TaskModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class CreateTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('create', [TaskModel::class, $this->project]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'title'       => 'required',
            'description' => 'required',
            'term'        => 'nullable|date_format:Y-m-d H:i:s',
            'duration'    => 'nullable|date_format:H:i:s',
            'assignee_id' => 'nullable|integer|exists:users,id',
            'status_id'   => 'required|integer|exists:statuses,id',
        ];
    }

    /**
     * Get the DTO representation of the request.
     *
     * @return \App\DataTransferObjects\Task
     */
    public function toDto(): Task
    {
        return Task::make(
            $this->title,
            $this->description,
            $this->term,
            $this->duration,
            $this->assignee_id,
            $this->user()->id,
            $this->project->id,
            $this->status_id
        );
    }
}
