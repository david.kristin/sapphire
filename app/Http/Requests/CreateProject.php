<?php declare(strict_types = 1);

namespace App\Http\Requests;

use App\DataTransferObjects\Project;
use Illuminate\Foundation\Http\FormRequest;

class CreateProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'name'        => 'required|string|unique:projects',
            'description' => 'nullable',
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return $this
            ->toDto()
            ->toArray();
    }

    /**
     * Get the DTO representation of the request.
     *
     * @return \App\DataTransferObjects\Project
     */
    public function toDto(): Project
    {
        return Project::make(
            $this->name,
            $this->description,
            $this->user()->id
        );
    }
}
