<?php declare(strict_types = 1);

namespace App\Exceptions;

class InvalidCredentialsException extends ApiException
{
    public function __construct()
    {
        $this->message = __('exceptions.invalid_credentials');
        $this->code = 401;

        parent::__construct();
    }
}
