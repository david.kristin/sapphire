<?php declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public const BACKLOG = 1;
    public const TODO = 2;
    public const SPRINT = 3;
    public const PROGRESS = 4;
    public const TESTING = 5;
    public const DONE = 6;
}
