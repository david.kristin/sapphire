<?php declare(strict_types = 1);

namespace App\Services;

use App\Contracts\MentionExtractor;

final class NameMentionExtractor implements MentionExtractor
{
    /**
     * Get all matched strings.
     *
     * @param string $content
     * @return array<int, string>
     */
    public function matches(string $content): array
    {
        preg_match_all("/(?<=\@)[\w\-]+/", $content, $matches);

        return $matches[0];
    }
}
