<?php declare(strict_types = 1);

namespace App\Services;

use App\Contracts\MentionExtractor;
use App\Contracts\MentionReplacer;

final class Mentions
{
    /** @var \App\Contracts\MentionExtractor */
    private $extractor;

    /** @var \App\Contracts\MentionReplacer */
    private $replacer;

    public function __construct(MentionExtractor $extractor, MentionReplacer $replacer)
    {
        $this->extractor = $extractor;
        $this->replacer = $replacer;
    }

    public function replace(string $content): string
    {
        $matches = $this->extractor->matches($content);

        return $this->replacer->replace($matches, $content);
    }
}
