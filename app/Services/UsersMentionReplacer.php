<?php declare(strict_types = 1);

namespace App\Services;

use App\Contracts\MentionReplacer;
use App\Contracts\Repositories\UserRepository;

final class UsersMentionReplacer implements MentionReplacer
{
    /** @var \App\Contracts\Repositories\UserRepository */
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * Get replaced content.
     *
     * @param array<int, string>  $matches
     * @param string              $content
     * @return string
     */
    public function replace(array $matches, string $content): string
    {
        $this
            ->user
            ->findWhereIn('name', $matches)
            ->pluck('name', 'id')
            ->each(static function (string $name, int $id) use (&$content): void {
                $mention = "@{$name}";

                $content = str_replace($mention, "[{$id}]{$mention}[/{$id}]", $content);
            });

        return $content;
    }
}
