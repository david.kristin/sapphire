<?php declare(strict_types = 1);

namespace App\Policies;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the task.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Project $project
     * @return bool
     */
    public function view(User $user, Project $project): bool
    {
        return $this->isProjectAuthor($project, $user) ||
            $this->isMember($project, $user);
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Project $project
     * @return bool
     */
    public function create(User $user, Project $project): bool
    {
        return $this->isProjectAuthor($project, $user) ||
            $this->isMember($project, $user);
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Task $task
     * @return bool
     */
    public function update(User $user, Task $task): bool
    {
        return $this->isTaskAuthor($task, $user);
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Task $task
     * @return bool
     */
    public function delete(User $user, Task $task): bool
    {
        return $this->isTaskAuthor($task, $user);
    }

    private function isTaskAuthor(Task $task, User $user): bool
    {
        return $task->author_id === $user->id;
    }

    private function isProjectAuthor(Project $project, User $user): bool
    {
        return $project->user_id === $user->id;
    }

    private function isMember(Project $project, User $user): bool
    {
        return $project
            ->members()
            ->where('user_id', $user->id)
            ->exists();
    }
}
