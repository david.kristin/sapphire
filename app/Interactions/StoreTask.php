<?php declare(strict_types = 1);

namespace App\Interactions;

use App\Contracts\Interactions\StoreTask as StoreTaskContract;
use App\Contracts\Repositories\TaskRepository;
use App\DataTransferObjects\Task;
use App\Models\Task as TaskModel;
use App\Services\Mentions;

class StoreTask implements StoreTaskContract
{
    /** @var \App\Contracts\Repositories\TaskRepository */
    private $task;

    /** @var \App\Services\Mentions */
    private $mentions;

    public function __construct(TaskRepository $task, Mentions $mentions)
    {
        $this->task = $task;
        $this->mentions = $mentions;
    }

    public function handle(Task $task): TaskModel
    {
        $task->description = $this->mentions->replace($task->description);

        return $this->task->create($task->toArray());
    }
}
