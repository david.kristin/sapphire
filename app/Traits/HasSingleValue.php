<?php declare(strict_types = 1);

namespace App\Traits;

use App\Contracts\ValueObject;

trait HasSingleValue
{
    /**
     * Compares the object to the passed object.
     *
     * @param \App\Contracts\ValueObject $object
     * @return bool
     */
    public function equals(ValueObject $object): bool
    {
        return $this->toNative() === $object->toNative();
    }
}
