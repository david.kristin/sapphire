<?php declare(strict_types = 1);

namespace App\Gates;

use App\Models\User;

class ChangePasswordGate
{
    /**
     * Determine if the current user is authorized to change a password.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return bool
     */
    public function authorize(User $user, User $model): bool
    {
        return $user->id === $model->id;
    }
}
