<?php declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repositories\ProjectRepository;
use App\Models\Project;
use Prettus\Repository\Eloquent\BaseRepository;

final class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Project::class;
    }
}
