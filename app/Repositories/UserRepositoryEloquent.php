<?php declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repositories\UserRepository;
use App\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;

final class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return User::class;
    }
}
