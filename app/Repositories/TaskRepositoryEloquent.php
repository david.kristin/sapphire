<?php declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repositories\TaskRepository;
use App\Models\Task;
use Prettus\Repository\Eloquent\BaseRepository;

final class TaskRepositoryEloquent extends BaseRepository implements TaskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Task::class;
    }
}
