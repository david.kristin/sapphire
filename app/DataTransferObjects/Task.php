<?php declare(strict_types = 1);

namespace App\DataTransferObjects;

use App\ValueObjects\Id;
use DateTimeImmutable;
use Spatie\DataTransferObject\DataTransferObject;

final class Task extends DataTransferObject
{
    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var \DateTimeImmutable|null */
    public $term;

    /** @var \DateTimeImmutable|null */
    public $duration;

    /** @var \App\ValueObjects\Id|null*/
    public $assignee_id;

    /** @var \App\ValueObjects\Id */
    public $author_id;

    /** @var \App\ValueObjects\Id */
    public $project_id;

    /** @var \App\ValueObjects\Id */
    public $status_id;

    public static function make(
        string $title,
        string $description,
        ?string $term,
        ?string $duration,
        ?int $assigneeId,
        int $authorId,
        int $projectId,
        int $statusId
    ): self {
        $term = is_null($term) ?
            null :
            new DateTimeImmutable($term);

        $duration = is_null($duration) ?
            null :
            new DateTimeImmutable($duration);

        $assigneeId = is_null($assigneeId) ?
            null :
            new Id($assigneeId);

        return new self([
            'title'       => $title,
            'description' => $description,
            'term'        => $term,
            'duration'    => $duration,
            'assignee_id' => $assigneeId,
            'author_id'   => new Id($authorId),
            'project_id'  => new Id($projectId),
            'status_id'   => new Id($statusId),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string|int|null>
     */
    public function toArray(): array
    {
        return [
            'title'       => $this->title,
            'description' => $this->description,
            'term'        => optional($this->term)->format('Y-m-d H:i:s'),
            'duration'    => optional($this->duration)->format('H:i:s'),
            'assignee_id' => optional($this->assignee_id)->toNative(),
            'author_id'   => $this->author_id->toNative(),
            'project_id'  => $this->project_id->toNative(),
            'status_id'   => $this->status_id->toNative(),
        ];
    }
}
