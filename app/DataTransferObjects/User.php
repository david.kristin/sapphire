<?php declare(strict_types = 1);

namespace App\DataTransferObjects;

use App\ValueObjects\Email;
use App\ValueObjects\Password;
use Spatie\DataTransferObject\DataTransferObject;

final class User extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var \App\ValueObjects\Email */
    public $email;

    /** @var \App\ValueObjects\Password */
    public $password;

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return \App\DataTransferObjects\User
     */
    public static function make(string $name, string $email, string $password): self
    {
        return new self([
            'name'     => $name,
            'email'    => new Email($email),
            'password' => new Password($password),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return [
            'name'     => $this->name,
            'email'    => $this->email->toNative(),
            'password' => bcrypt($this->password->toNative()),
        ];
    }
}
