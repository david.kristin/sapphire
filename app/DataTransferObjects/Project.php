<?php declare(strict_types = 1);

namespace App\DataTransferObjects;

use App\ValueObjects\Id;
use Spatie\DataTransferObject\DataTransferObject;

final class Project extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var string|null */
    public $description;

    /** @var \App\ValueObjects\Id */
    public $user_id;

    /**
     * @param string      $name
     * @param string|null $description
     * @param int         $userId
     * @return \App\DataTransferObjects\Project
     */
    public static function make(string $name, ?string $description, int $userId): self
    {
        return new self([
            'name'        => $name,
            'description' => $description,
            'user_id'     => new Id($userId),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string|int|null>
     */
    public function toArray(): array
    {
        return [
            'name'        => $this->name,
            'description' => $this->description,
            'user_id'     => $this->user_id->toNative(),
        ];
    }
}
