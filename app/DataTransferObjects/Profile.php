<?php declare(strict_types = 1);

namespace App\DataTransferObjects;

use App\ValueObjects\Email;
use Spatie\DataTransferObject\DataTransferObject;

final class Profile extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var \App\ValueObjects\Email */
    public $email;

    /**
     * @param string $name
     * @param string $email
     * @return \App\DataTransferObjects\Profile
     */
    public static function make(string $name, string $email): self
    {
        return new self([
            'name'  => $name,
            'email' => new Email($email),
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array<string, string>
     */
    public function toArray(): array
    {
        return [
            'name'  => $this->name,
            'email' => $this->email->toNative(),
        ];
    }
}
