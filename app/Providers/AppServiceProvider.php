<?php

namespace App\Providers;

use App\Contracts\MentionExtractor;
use App\Contracts\MentionReplacer;
use App\Services\NameMentionExtractor;
use App\Services\UsersMentionReplacer;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MentionExtractor::class, NameMentionExtractor::class);
        $this->app->bind(MentionReplacer::class, UsersMentionReplacer::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
