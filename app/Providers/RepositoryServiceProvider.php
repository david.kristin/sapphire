<?php declare(strict_types = 1);

namespace App\Providers;

use App\Contracts\Repositories as Contracts;
use App\Repositories;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array<string, string>
     */
    public $bindings = [
        Contracts\UserRepository::class    => Repositories\UserRepositoryEloquent::class,
        Contracts\ProjectRepository::class => Repositories\ProjectRepositoryEloquent::class,
        Contracts\TaskRepository::class    => Repositories\TaskRepositoryEloquent::class,
    ];

    /**
     * Get the services provided by the provider.
     *
     * @return array<string>
     */
    public function provides(): array
    {
        return array_keys($this->bindings);
    }
}
