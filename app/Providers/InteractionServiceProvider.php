<?php declare(strict_types = 1);

namespace App\Providers;

use App\Contracts\Interactions as Contracts;
use App\Interactions;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class InteractionServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array<string, string>
     */
    public $bindings = [
        Contracts\StoreTask::class => Interactions\StoreTask::class,
    ];

    /**
     * Get the services provided by the provider.
     *
     * @return array<string>
     */
    public function provides(): array
    {
        return array_keys($this->bindings);
    }
}
