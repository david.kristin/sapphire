<?php

namespace App\Providers;

use App\Gates\ChangePasswordGate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<string, string>
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * The gate mappings for the application.
     *
     * @var array<string, string>
     */
    protected $gates = [
        'changePassword' => ChangePasswordGate::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();
        $this->registerGates();

        Gate::guessPolicyNamesUsing(function (string $modelClass) {
            $model = class_basename($modelClass);

            return ["App\\Policies\\{$model}Policy"];
        });
    }

    /**
     * Register the application's gates.
     *
     * @return void
     */
    private function registerGates(): void
    {
        foreach ($this->gates as $name => $class) {
            Gate::define($name, "{$class}@authorize");
        }
    }
}
