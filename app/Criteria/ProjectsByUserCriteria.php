<?php declare(strict_types = 1);

namespace App\Criteria;

use App\ValueObjects\Id;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class ProjectsByUserCriteria implements CriteriaInterface
{
    /** @var int */
    private $userId;

    public function __construct(Id $userId)
    {
        $this->userId = $userId->toNative();
    }

    /**
     * Apply criteria in query repository.
     *
     * @param \App\Models\Project                               $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        $model = $model
            ->where('user_id', $this->userId)
            ->orWhereHas('members', function (Builder $query): void {
                $query->where('user_id', $this->userId);
            });

        return $model;
    }
}
