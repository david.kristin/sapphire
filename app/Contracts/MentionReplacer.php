<?php declare(strict_types = 1);

namespace App\Contracts;

interface MentionReplacer
{
    /**
     * Get replaced content.
     *
     * @param array<int, string> $matches
     * @param string             $content
     * @return string
     */
    public function replace(array $matches, string $content): string;
}
