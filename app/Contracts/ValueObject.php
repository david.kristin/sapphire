<?php declare(strict_types = 1);

namespace App\Contracts;

interface ValueObject
{
    /**
     * Compares the object to the passed object.
     *
     * @param \App\Contracts\ValueObject $object
     * @return bool
     */
    public function equals(ValueObject $object): bool;

    /**
     * Get the native representation of the object.
     *
     * @return mixed
     */
    public function toNative();
}
