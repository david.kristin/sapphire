<?php declare(strict_types = 1);

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UserRepository extends RepositoryInterface
{
    //
}
