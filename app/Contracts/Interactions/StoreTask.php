<?php declare(strict_types = 1);

namespace App\Contracts\Interactions;

use App\DataTransferObjects\Task;

interface StoreTask
{
    /**
     * @param \App\DataTransferObjects\Task $task
     * @return \App\Models\Task
     */
    public function handle(Task $task);
}
