<?php declare(strict_types = 1);

namespace App\Contracts;

interface MentionExtractor
{
    /**
     * Get all matched strings.
     *
     * @param string $content
     * @return array<int, string>
     */
    public function matches(string $content): array;
}
