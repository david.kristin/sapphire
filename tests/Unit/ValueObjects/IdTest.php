<?php declare(strict_types = 1);

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\Id;
use Tests\TestCase;

class IdTest extends TestCase
{
    /** @test */
    public function an_id_equals_to_a_native_value(): void
    {
        $userId = new Id($id = 1);

        $this->assertEquals($id, $userId->toNative());
    }

    /** @test */
    public function emails_are_compared_by_their_values(): void
    {
        $johnId = new Id(1);
        $janId = new Id(2);

        $this->assertFalse($johnId->equals($janId));
        $this->assertTrue($johnId->equals($johnId));
    }

    /**
     * @test
     * @expectedException \App\Exceptions\NegativeIdException
     */
    public function an_id_must_be_a_positive_integer(): void
    {
        new Id(-1);
    }
}
