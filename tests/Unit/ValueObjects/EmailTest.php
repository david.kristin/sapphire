<?php declare(strict_types = 1);

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\Email;
use Tests\TestCase;

class EmailTest extends TestCase
{
    /** @test */
    public function a_valid_email_equals_to_a_native_value(): void
    {
        $john = new Email($email = 'john@doe.com');

        $this->assertEquals($email, $john->toNative());
    }

    /** @test */
    public function emails_are_compared_by_their_values(): void
    {
        $john = new Email('john@doe.com');
        $jan = new Email('jan@hoe.com');

        $this->assertFalse($john->equals($jan));
        $this->assertTrue($john->equals($john));
    }

    /**
     * @test
     * @expectedException \App\Exceptions\InvalidEmailException
     */
    public function an_email_must_contain_an_at_sign(): void
    {
        new Email('doe.com');
    }

    /**
     * @test
     * @expectedException \App\Exceptions\InvalidEmailException
     */
    public function an_email_must_contain_a_dot(): void
    {
        new Email('john@doe');
    }

    /**
     * @test
     * @expectedException \App\Exceptions\InvalidEmailException
     */
    public function an_email_must_contain_a_tld(): void
    {
        new Email('john@doe.');
    }

    /**
     * @test
     * @expectedException \App\Exceptions\InvalidEmailException
     */
    public function an_email_must_contain_a_domain(): void
    {
        new Email('john@.com');
    }

    /**
     * @test
     * @expectedException \App\Exceptions\InvalidEmailException
     */
    public function an_email_must_contain_a_part_before_an_at_sign(): void
    {
        new Email('@doe.com');
    }
}
