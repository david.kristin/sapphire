<?php declare(strict_types = 1);

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\Password;
use Tests\TestCase;

class PasswordTest extends TestCase
{
    /** @test */
    public function a_valid_password_equals_to_a_native_value(): void
    {
        $password = new Password($strongPassword = 'str0ng!Pa44w0rd');

        $this->assertEquals($strongPassword, $password->toNative());
    }

    /** @test */
    public function passwords_are_compared_by_their_values(): void
    {
        $password = new Password('password');
        $strongPassword = new Password('str0ng!Pa44w0rd');

        $this->assertFalse($password->equals($strongPassword));
        $this->assertTrue($password->equals($password));
    }

    /**
     * @test
     * @expectedException \App\Exceptions\PasswordTooShortException
     */
    public function a_password_must_be_at_least_8_characters_long(): void
    {
        new Password('secret');
    }
}
