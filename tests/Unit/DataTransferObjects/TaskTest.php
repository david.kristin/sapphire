<?php declare(strict_types = 1);

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\Task;
use App\ValueObjects\Id;
use DateTimeImmutable;
use Tests\TestCase;

class TaskTest extends TestCase
{
    /** @test */
    public function check_the_objects_properties_types(): void
    {
        $task = $this->createTask($this->getTask());

        $this->assertNullableValues($task);
        $this->assertIsString($task->title);
        $this->assertIsString($task->description);
        $this->assertInstanceOf(Id::class, $task->assignee_id);
        $this->assertInstanceOf(Id::class, $task->author_id);
        $this->assertInstanceOf(Id::class, $task->project_id);
        $this->assertInstanceOf(Id::class, $task->status_id);
    }

    /** @test */
    public function an_array_representation_returns_valid_data(): void
    {
        $task = $this->getTask();
        $taskDto = $this
            ->createTask($task)
            ->toArray();

        $this->assertCount(8, $taskDto);

        $this->assertEquals($task['title'], $taskDto['title']);
        $this->assertEquals($task['description'], $taskDto['description']);
        $this->assertEquals($task['term'], $taskDto['term']);
        $this->assertEquals($task['duration'], $taskDto['duration']);
        $this->assertEquals($task['assignee_id'], $taskDto['assignee_id']);
        $this->assertEquals($task['author_id'], $taskDto['author_id']);
        $this->assertEquals($task['project_id'], $taskDto['project_id']);
        $this->assertEquals($task['status_id'], $taskDto['status_id']);
    }

    private function getTask(): array
    {
        return factory(\App\Models\Task::class)->raw([
            'assignee_id' => 1,
            'author_id'   => 1,
            'project_id'  => 1,
            'status_id'   => 1,
        ]);
    }

    private function createTask(array $task): Task
    {
        return Task::make(
            $task['title'],
            $task['description'],
            $task['term'],
            $task['duration'],
            $task['assignee_id'],
            $task['author_id'],
            $task['project_id'],
            $task['status_id']
        );
    }

    private function assertNullableValues(Task $task): void
    {
        is_null($task->term) ?
            $this->assertNull($task->term) :
            $this->assertInstanceOf(DateTimeImmutable::class, $task->term);

        is_null($task->duration) ?
            $this->assertNull($task->duration) :
            $this->assertInstanceOf(DateTimeImmutable::class, $task->duration);
    }
}
