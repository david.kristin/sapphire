<?php declare(strict_types = 1);

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\Profile;
use App\Models\User;
use App\ValueObjects\Email;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    /** @test */
    public function check_the_objects_properties_types(): void
    {
        $profile = $this->createProfile($this->getUser());

        $this->assertIsString($profile->name);
        $this->assertInstanceOf(Email::class, $profile->email);
    }

    /** @test */
    public function an_array_representation_returns_valid_data(): void
    {
        $user = $this->getUser();
        $profile = $this
            ->createProfile($user)
            ->toArray();

        $this->assertCount(2, $profile);

        $this->assertEquals($user['name'], $profile['name']);
        $this->assertEquals($user['email'], $profile['email']);
    }

    private function getUser(): array
    {
        return factory(User::class)->raw();
    }

    private function createProfile(array $user): Profile
    {
        return Profile::make(
            $user['name'],
            $user['email']
        );
    }
}
