<?php declare(strict_types = 1);

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\User;
use App\ValueObjects\Email;
use App\ValueObjects\Password;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function check_the_objects_properties_types(): void
    {
        $user = $this->createUser($this->getUser());

        $this->assertIsString($user->name);
        $this->assertInstanceOf(Email::class, $user->email);
        $this->assertInstanceOf(Password::class, $user->password);
    }

    /** @test */
    public function an_array_representation_returns_valid_data(): void
    {
        $user = $this->getUser();
        $userDto = $this
            ->createUser($user)
            ->toArray();

        $this->assertCount(3, $userDto);

        $this->assertEquals($user['name'], $userDto['name']);
        $this->assertEquals($user['email'], $userDto['email']);

        $this->assertTrue(
            Hash::check($user['password'], $userDto['password'])
        );
    }

    private function getUser(): array
    {
        return factory(\App\Models\User::class)->raw();
    }

    private function createUser(array $user): User
    {
        return User::make(
            $user['name'],
            $user['email'],
            $user['password']
        );
    }
}
