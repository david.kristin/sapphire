<?php declare(strict_types = 1);

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\Project;
use App\ValueObjects\Id;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    /** @test */
    public function check_the_objects_properties_types(): void
    {
        $project = $this->createProject($this->getProject());

        $this->assertNullableValues($project);
        $this->assertIsString($project->name);
        $this->assertInstanceOf(Id::class, $project->user_id);
    }

    /** @test */
    public function an_array_representation_returns_valid_data(): void
    {
        $project = $this->getProject();
        $projectDto = $this
            ->createProject($project)
            ->toArray();

        $this->assertCount(3, $projectDto);

        $this->assertEquals($project['name'], $projectDto['name']);
        $this->assertEquals($project['description'], $projectDto['description']);
        $this->assertEquals($project['user_id'], $projectDto['user_id']);
    }

    private function getProject(): array
    {
        return factory(\App\Models\Project::class)->raw([
            'user_id' => 1,
        ]);
    }

    private function createProject(array $project): Project
    {
        return Project::make(
            $project['name'],
            $project['description'],
            $project['user_id']
        );
    }

    private function assertNullableValues(Project $project): void
    {
        is_null($project->description) ?
            $this->assertNull($project->description) :
            $this->assertIsString($project->description);
    }
}
