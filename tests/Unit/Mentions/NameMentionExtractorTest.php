<?php declare(strict_types = 1);

namespace Tests\Unit\Mentions;

use App\Services\NameMentionExtractor;
use Tests\TestCase;

class NameMentionExtractorTest extends TestCase
{
    /** @var \App\Services\NameMentionExtractor */
    private $extractor;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->extractor = $this->app->make(NameMentionExtractor::class);
    }

    /** @test */
    public function a_mention_is_returned_without_at_sign(): void
    {
        $matches = $this->extractor->matches('Hello, @JohnDoe');

        $this->assertEquals(['JohnDoe'], $matches);
    }

    /** @test */
    public function multiple_mentions_are_matched(): void
    {
        $matches = $this->extractor->matches('Hello, @John_Doe and @jane-doe @johnRoe1');

        $this->assertEquals(
            [
                'John_Doe',
                'jane-doe',
                'johnRoe1',
            ],
            $matches
        );
    }

    /** @test */
    public function an_empty_array_is_returned_if_there_are_not_matches(): void
    {
        $matches = $this->extractor->matches('Hello');

        $this->assertEquals([], $matches);
    }
}
