<?php declare(strict_types = 1);

namespace Tests\Unit\Mentions;

use App\Contracts\Repositories\UserRepository;
use App\Services\UsersMentionReplacer;
use Illuminate\Support\Collection;
use Mockery\MockInterface;
use Tests\TestCase;

class UsersMentionReplacerTest extends TestCase
{
    /** @test */
    public function all_occurrences_of_the_same_match_are_replaced(): void
    {
        $matches = ['JohnDoe'];

        $content = $this
            ->replacer($matches, $this->mapUsers($matches))
            ->replace($matches, 'Hello, @JohnDoe & @JohnDoe');

        $this->assertEquals('Hello, [1]@JohnDoe[/1] & [1]@JohnDoe[/1]', $content);
    }

    /** @test */
    public function only_matches_existing_in_the_database_are_replaced(): void
    {
        $matches = [
            'JohnDoe',
            'JaneDoe',
        ];

        $content = $this
            ->replacer($matches, $this->mapUsers(['JohnDoe']))
            ->replace($matches, 'Hello, @JohnDoe and @JaneDoe');

        $this->assertEquals('Hello, [1]@JohnDoe[/1] and @JaneDoe', $content);
    }

    /** @test */
    public function multiple_matches_are_replaced(): void
    {
        $matches = [
            'JohnDoe',
            'JaneDoe',
        ];

        $content = $this
            ->replacer($matches, $this->mapUsers($matches))
            ->replace($matches, 'Hello, @JohnDoe and @JaneDoe');

        $this->assertEquals('Hello, [1]@JohnDoe[/1] and [2]@JaneDoe[/2]', $content);
    }

    /** @test */
    public function no_replacement_is_made_if_there_is_not_a_match(): void
    {
        $content = $this
            ->replacer([], $this->mapUsers([]))
            ->replace([], 'Hello');

        $this->assertEquals('Hello', $content);
    }

    private function replacer(array $matches, Collection $users): UsersMentionReplacer
    {
        $this->mock(UserRepository::class, static function (MockInterface $mock) use ($matches, $users): void {
            $mock
                ->shouldReceive('findWhereIn')
                ->with('name', $matches)
                ->once()
                ->andReturn($users);
        });

        return $this->app->make(UsersMentionReplacer::class);
    }

    private function mapUsers(array $users): Collection
    {
        return collect($users)
            ->map(static function (string $name) {
                static $id = 0;

                return [
                    'id'   => $id += 1,
                    'name' => $name,
                ];
            });
    }
}
