<?php declare(strict_types = 1);

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class ApiTestCase extends TestCase
{
    use RefreshDatabase;
    use HandlesApiAuthentication;

    /** @var \App\Models\User */
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = $this->user();
    }

    /**
     * @param array<string, string|int|float> $attributes
     * @return \App\Models\User
     */
    protected function user(array $attributes = []): User
    {
        return factory(User::class)->create($attributes);
    }
}
