<?php declare(strict_types = 1);

namespace Tests\Feature\Api\Task;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Collection;
use Tests\ApiTestCase;

class GetTasksTest extends ApiTestCase
{
    /** @test */
    public function the_tasks_belonging_to_a_project_are_returned(): void
    {
        $project = factory(Project::class)
            ->state('tasks')
            ->create([
                'user_id' => $this->user->id,
            ]);

        $this
            ->listTasks($project)
            ->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertExactJson(
                $this->tasksStructure($project->tasks)
            );
    }

    /** @test */
    public function the_project_members_can_list_the_tasks(): void
    {
        $project = factory(Project::class)
            ->state('tasks')
            ->create();

        $project
            ->members()
            ->attach($this->user->id);

        $this
            ->listTasks($project)
            ->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertExactJson(
                $this->tasksStructure($project->tasks)
            );
    }

    /** @test */
    public function an_unauthorized_user_cannot_list_the_tasks(): void
    {
        $project = factory(Project::class)->create();

        $this
            ->listTasks($project)
            ->assertStatus(403);
    }

    private function listTasks(Project $project): TestResponse
    {
        return $this
            ->setAuthorizationHeader($this->user)
            ->getJson(route('api.v1.listTasks', $project));
    }

    private function tasksStructure(Collection $tasks): array
    {
        return $tasks
            ->map(function (Task $task) {
                return $this->taskStructure($task);
            })
            ->pipe(static function (Collection $tasks) {
                return collect(['data' => $tasks]);
            })
            ->toArray();
    }

    private function taskStructure(Task $task): array
    {
        return [
            'id'       => $task->id,
            'title'    => $task->title,
            'assignee' => [
                'id'    => $task->assignee->id,
                'name'  => $task->assignee->name,
                'email' => $task->assignee->email,
            ],
            'status'   => [
                'id'   => $task->status->id,
                'name' => $task->status->name,
            ],
        ];
    }
}
