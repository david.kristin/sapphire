<?php declare(strict_types = 1);

namespace Tests\Feature\Api\Task;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\ApiTestCase;

class CreateTaskTest extends ApiTestCase
{
    /** @test */
    public function a_task_is_stored(): void
    {
        $project = $this->getProject($this->user);
        $task = $this->getTask();

        $this
            ->createTask($project, $task)
            ->assertStatus(201);

        $this->assertDatabaseHas('tasks', array_merge($task, [
            'author_id'  => $this->user->id,
            'project_id' => $project->id,
        ]));
    }

    /** @test */
    public function a_project_is_stored_with_the_nullable_values(): void
    {
        $project = $this->getProject($this->user);
        $task = $this->getTask([
            'term'        => null,
            'duration'    => null,
            'assignee_id' => null,
        ]);

        $this
            ->createTask($project, $task)
            ->assertStatus(201);

        $this->assertDatabaseHas('tasks', array_merge($task, [
            'author_id'  => $this->user->id,
            'project_id' => $project->id,
        ]));
    }

    /** @test */
    public function the_mentions_are_replaced(): void
    {
        $description = "Hello, @{$this->user->name}";
        $replacedDescription = "Hello, [{$this->user->id}]@{$this->user->name}[/{$this->user->id}]";

        $project = $this->getProject($this->user);
        $task = $this->getTask(compact('description'));

        $this
            ->createTask($project, $task)
            ->assertStatus(201);

        $task = Task::latest('id')->first();

        $this->assertEquals($replacedDescription, $task->description);
    }

    /** @test */
    public function an_unauthorized_user_cannot_create_a_task(): void
    {
        $project = $this->getProject($this->user());

        $this
            ->createTask($project, $this->getTask())
            ->assertStatus(403);
    }

    private function createTask(Project $project, array $task): TestResponse
    {
        return $this
            ->setAuthorizationHeader($this->user)
            ->postJson(
                route('api.v1.createTask', $project),
                $task
            );
    }

    private function getProject(User $user): Project
    {
        return factory(Project::class)->create([
            'user_id' => $user->id,
        ]);
    }

    private function getTask(array $attributes = []): array
    {
        return factory(Task::class)->raw($attributes);
    }
}
