<?php declare(strict_types = 1);

namespace Tests\Feature\Api\User;

use Tests\ApiTestCase;

class GetProfileTest extends ApiTestCase
{
    /** @test */
    public function a_profile_is_returned_for_a_specified_user(): void
    {
        $this
            ->setAuthorizationHeader($this->user)
            ->getJson(route('api.v1.profile', $this->user))
            ->assertStatus(200)
            ->assertExactJson([
                'data' => [
                    'id'    => $this->user->id,
                    'name'  => $this->user->name,
                    'email' => $this->user->email,
                ],
            ]);
    }
}
