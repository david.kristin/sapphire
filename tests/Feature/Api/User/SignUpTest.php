<?php declare(strict_types = 1);

namespace Tests\Feature\Api\User;

use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Hash;
use Tests\ApiTestCase;

class SignUpTest extends ApiTestCase
{
    /** @test */
    public function a_user_is_stored(): void
    {
        $user = $this->getUser();

        $this
            ->registerUser($user)
            ->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'name'  => $user['name'],
            'email' => $user['email'],
        ]);
    }

    /** @test */
    public function a_password_is_hashed(): void
    {
        $user = $this->getUser();

        $this->registerUser($user);

        $registeredUser = User::latest('id')->first();

        $this->assertTrue(
            Hash::check($user['password'], $registeredUser->password)
        );
    }

    /** @test */
    public function a_name_must_be_unique(): void
    {
        $user = $this->user->toArray();

        $this
            ->registerUser($user)
            ->assertStatus(422)
            ->assertJsonValidationErrors('name');
    }

    /** @test */
    public function a_name_must_contain_only_alpha_dash_characters(): void
    {
        $user = $this->getUser('password', [
            'name' => 'user.name',
        ]);

        $this
            ->registerUser($user)
            ->assertStatus(422)
            ->assertJsonValidationErrors('name');
    }

    /** @test */
    public function an_email_must_be_unique(): void
    {
        $user = $this->user->toArray();

        $this
            ->registerUser($user)
            ->assertStatus(422)
            ->assertJsonValidationErrors('email');
    }

    /**
     * @param array $user
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function registerUser(array $user): TestResponse
    {
        return $this->postJson(
            route('api.v1.register'),
            $user
        );
    }

    /**
     * @param string $password
     * @param array  $attributes
     * @return array
     */
    private function getUser(string $password = 'password', array $attributes = []): array
    {
        return factory(User::class)->raw(
            array_merge($attributes, [
                'password'              => $password,
                'password_confirmation' => $password,
            ])
        );
    }
}
