<?php declare(strict_types = 1);

namespace Tests\Feature\Api\User;

use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\ApiTestCase;

class ChangePasswordTest extends ApiTestCase
{
    /** @var string */
    private $password;

    protected function setUp(): void
    {
        parent::setUp();

        $this->password = Str::random(8);
    }

    /** @test */
    public function a_password_is_changed(): void
    {
        $this
            ->changePassword(
                $this->user,
                $this->user,
                $this->password
            )
            ->assertStatus(204);

        $user = $this->user->fresh();

        $this->assertTrue(
            Hash::check($this->password, $user->password)
        );
    }

    /** @test */
    public function a_user_cannot_change_another_users_password(): void
    {
        $user = $this->user();

        $this
            ->changePassword(
                $this->user,
                $user,
                $this->password
            )
            ->assertStatus(403);
    }

    private function changePassword(User $authUser, User $user, string $password): TestResponse
    {
        return $this
            ->setAuthorizationHeader($authUser)
            ->putJson(
                route('api.v1.changePassword', $user),
                [
                    'password'              => $password,
                    'password_confirmation' => $password,
                ]
            );
    }
}
