<?php declare(strict_types = 1);

namespace Tests\Feature\Api\User;

use Tests\ApiTestCase;

class SignInTest extends ApiTestCase
{
    /** @test */
    public function a_token_is_returned_if_a_user_is_signed_in(): void
    {
        $this
            ->postJson(
                route('api.v1.login'),
                [
                    'email'    => $this->user->email,
                    'password' => 'password',
                ]
            )
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                ],
            ]);
    }
}
