<?php declare(strict_types = 1);

namespace Tests\Feature\Api\User;

use Illuminate\Foundation\Testing\TestResponse;
use Tests\ApiTestCase;

class RefreshTokenTest extends ApiTestCase
{
    /** @test */
    public function a_token_is_refreshed_if_the_refresh_ttl_has_not_passed(): void
    {
        $this
            ->refresh()
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                ],
            ])
            ->assertJsonFragment([
                'token_type' => 'bearer',
                'expires_in' => config('jwt.ttl') * 60,
            ]);
    }

    /**
     * @test
     * @expectedException \Tymon\JWTAuth\Exceptions\TokenExpiredException
     */
    public function a_token_is_not_refreshed_if_it_can_no_longer_be_refreshed(): void
    {
        $this->withoutExceptionHandling();

        config()->set('jwt.refresh_ttl', 0);

        $this->refresh();
    }

    private function refresh(): TestResponse
    {
        return $this
            ->setAuthorizationHeader($this->user)
            ->putJson(route('api.v1.refresh'));
    }
}
