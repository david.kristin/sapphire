<?php declare(strict_types = 1);

namespace Tests\Feature\Api\User;

use Tests\ApiTestCase;

class SignOutTest extends ApiTestCase
{
    /** @test */
    public function a_user_is_signed_out(): void
    {
        $this->login();

        $this
            ->deleteJson(route('api.v1.logout'))
            ->assertStatus(204);
    }

    private function login(): void
    {
        $this->postJson(
            route('api.v1.login'),
            [
                'email'    => $this->user->email,
                'password' => 'password',
            ]
        );
    }
}
