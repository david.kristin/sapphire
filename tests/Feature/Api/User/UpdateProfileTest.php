<?php declare(strict_types = 1);

namespace Tests\Feature\Api;

use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\ApiTestCase;

class UpdateProfileTest extends ApiTestCase
{
    /** @test */
    public function a_user_is_updated(): void
    {
        $user = factory(User::class)->raw();

        $this
            ->updateProfile(
                $this->user,
                $this->user,
                $user
            )
            ->assertStatus(204);

        $this->assertDatabaseHas('users', [
            'id'    => $this->user->id,
            'name'  => $user['name'],
            'email' => $user['email'],
        ]);
    }

    /** @test */
    public function a_unique_validation_ignores_the_user(): void
    {
        $this
            ->updateProfile(
                $this->user,
                $this->user,
                $this->user->toArray()
            )
            ->assertStatus(204);
    }

    /** @test */
    public function a_user_cannot_update_another_users_profile(): void
    {
        $user = $this->user();

        $this
            ->updateProfile(
                $this->user,
                $user,
                $this->user->toArray()
            )
            ->assertStatus(403);
    }

    /**
     * @param \App\Models\User $authUser
     * @param \App\Models\User $user
     * @param array            $data
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function updateProfile(User $authUser, User $user, array $data): TestResponse
    {
        return $this
            ->setAuthorizationHeader($authUser)
            ->putJson(
                route('api.v1.updateProfile', $user),
                $data
            );
    }
}
