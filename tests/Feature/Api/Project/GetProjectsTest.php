<?php declare(strict_types = 1);

namespace Tests\Feature\Api\Project;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Collection;
use Tests\ApiTestCase;

class GetProjectsTest extends ApiTestCase
{
    /** @test */
    public function the_projects_created_by_a_user_are_returned(): void
    {
        $projects = factory(Project::class, 3)->create([
            'user_id' => $this->user->id,
        ]);

        $this
            ->listProjects()
            ->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertExactJson(
                $this->projectsStructure($projects)
            );
    }

    /** @test */
    public function the_projects_a_user_is_a_member_of_are_returned(): void
    {
        $projects = factory(Project::class, 3)
            ->create()
            ->each(function (Project $project): void {
                $project
                    ->members()
                    ->attach($this->user->id);
            });

        $this
            ->listProjects()
            ->assertStatus(200)
            ->assertJsonCount(3, 'data')
            ->assertExactJson(
                $this->projectsStructure($projects)
            );
    }

    /** @test */
    public function the_projects_unavailable_for_a_user_are_not_returned(): void
    {
        $projects = factory(Project::class, 3)->create();

        $projects
            ->first()
            ->members()
            ->attach($this->user->id);

        $this
            ->listProjects()
            ->assertStatus(200)
            ->assertJsonCount(1, 'data')
            ->assertExactJson(
                $this->projectsStructure($projects->take(1))
            );
    }

    private function listProjects(): TestResponse
    {
        return $this
            ->setAuthorizationHeader($this->user)
            ->getJson(route('api.v1.listProjects'));
    }

    private function projectsStructure(Collection $projects): array
    {
        return $projects
            ->map(function (Project $project) {
                return $this->projectStructure($project);
            })
            ->pipe(static function (Collection $projects) {
                return collect(['data' => $projects]);
            })
            ->toArray();
    }

    private function projectStructure(Project $project): array
    {
        $members = $project
            ->members
            ->map(static function (User $user) {
                return [
                    'id'    => $user->id,
                    'email' => $user->email,
                    'name'  => $user->name,
                ];
            })
            ->toArray();

        return [
            'id'          => $project->id,
            'name'        => $project->name,
            'description' => $project->description,
            'members'     => $members,
        ];
    }
}
