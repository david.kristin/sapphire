<?php declare(strict_types = 1);

namespace Tests\Feature\Api\Project;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Arr;
use Tests\ApiTestCase;

class UpdateMembersTest extends ApiTestCase
{
    /** @test */
    public function the_project_members_are_updated(): void
    {
        $member = $this->user();
        $project = $this->getProjectWithMembers();

        $this
            ->updateMembers($this->user, $project, $member)
            ->assertStatus(204);

        $this->assertDatabaseHas('members', [
            'project_id' => $project->id,
            'user_id'    => $member->id,
        ]);
    }

    /** @test */
    public function the_project_members_are_detached(): void
    {
        $member = $this->user();
        $project = $this->getProjectWithMembers();
        $detachedMember = $project
            ->members()
            ->first();

        $this
            ->updateMembers($this->user, $project, $member)
            ->assertStatus(204);

        $this->assertDatabaseMissing('members', [
            'project_id' => $project->id,
            'user_id'    => $detachedMember->id,
        ]);

        $this->assertCount(1, $project->members);
    }

    /** @test */
    public function an_unauthorized_user_cannot_update_the_project_members(): void
    {
        $user = $this->user();
        $project = factory(Project::class)->create();

        $this
            ->updateMembers($user, $project, $user)
            ->assertStatus(403);
    }

    /** @test */
    public function a_members_array_must_contain_only_existing_users(): void
    {
        $member = factory(User::class)->make();
        $project = $this->getProjectWithMembers();

        $this
            ->updateMembers($this->user, $project, $member)
            ->assertStatus(422)
            ->assertJsonValidationErrors('members');
    }

    private function updateMembers(User $user, Project $project, User $member): TestResponse
    {
        return $this
            ->setAuthorizationHeader($user)
            ->putJson(
                route('api.v1.updateMembers', $project),
                ['members' => Arr::wrap($member->id)]
            );
    }

    private function getProjectWithMembers(): Project
    {
        return factory(Project::class)
            ->state('members')
            ->create([
                'user_id' => $this->user->id,
            ]);
    }
}
