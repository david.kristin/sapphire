<?php declare(strict_types = 1);

namespace Tests\Feature\Api\Project;

use App\Models\Project;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\ApiTestCase;

class CreateProjectTest extends ApiTestCase
{
    /** @test */
    public function a_project_is_stored(): void
    {
        $project = $this->getProject();

        $this
            ->createProject($project)
            ->assertStatus(201);

        $this->assertDatabaseHas('projects', [
            'name'        => $project['name'],
            'description' => $project['description'],
            'user_id'     => $this->user->id,
        ]);
    }

    /** @test */
    public function a_project_is_stored_with_a_nullable_description(): void
    {
        $project = $this->getProject([
            'description' => null,
        ]);

        $this
            ->createProject($project)
            ->assertStatus(201);

        $this->assertDatabaseHas('projects', [
            'name'        => $project['name'],
            'description' => null,
            'user_id'     => $this->user->id,
        ]);
    }

    private function createProject(array $project): TestResponse
    {
        return $this
            ->setAuthorizationHeader($this->user)
            ->postJson(
                route('api.v1.createProject'),
                $project
            );
    }

    private function getProject(array $attributes = []): array
    {
        return factory(Project::class)->raw($attributes);
    }
}
