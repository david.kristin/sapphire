<?php declare(strict_types = 1);

namespace Tests\Feature\Api\Project;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\ApiTestCase;

class DeleteProjectTest extends ApiTestCase
{
    /** @test */
    public function a_project_is_deleted(): void
    {
        $project = $this->getProject($this->user->id);

        $this
            ->deleteProject($this->user, $project)
            ->assertStatus(204);

        $this->assertDatabaseMissing('projects', $project->toArray());
    }

    /** @test */
    public function an_unauthorized_user_cannot_delete_a_project(): void
    {
        $project = $this->getProject($this->user()->id);

        $this
            ->deleteProject($this->user, $project)
            ->assertStatus(403);
    }

    private function deleteProject(User $user, Project $project): TestResponse
    {
        return $this
            ->setAuthorizationHeader($user)
            ->deleteJson(
                route('api.v1.deleteProject', compact('project'))
            );
    }

    private function getProject(int $userId): Project
    {
        return factory(Project::class)->create([
            'user_id' => $userId,
        ]);
    }
}
