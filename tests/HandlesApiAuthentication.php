<?php declare(strict_types = 1);

namespace Tests;

use Tymon\JWTAuth\Contracts\JWTSubject;

trait HandlesApiAuthentication
{
    /**
     * @param \Tymon\JWTAuth\Contracts\JWTSubject $user
     * @param string|null                         $driver
     * @return $this
     */
    public function setAuthorizationHeader(JWTSubject $user, ?string $driver = null)
    {
        $token = auth()
            ->guard($driver)
            ->fromUser($user);

        auth()
            ->guard($driver)
            ->setToken($token)
            ->user();

        $this->withHeader('Authorization', 'Bearer ' . $token);

        return $this;
    }
}
